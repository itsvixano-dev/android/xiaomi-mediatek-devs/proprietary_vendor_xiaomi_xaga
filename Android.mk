#
# Automatically generated file. DO NOT MODIFY
#

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),xaga)

$(call add-radio-file-sha1-checked,radio/apusys.img,5d47c2b7cea754c953f8cf20d66fb0341e63e734)
$(call add-radio-file-sha1-checked,radio/audio_dsp.img,584c2dbe794a71a46a023391a3efdebf2b9d589d)
$(call add-radio-file-sha1-checked,radio/ccu.img,fefc49ec293fe8c0164b83db209e422902233131)
$(call add-radio-file-sha1-checked,radio/dpm.img,76804632a42cd94fb09d163b25efd5286603d65b)
$(call add-radio-file-sha1-checked,radio/dtbo.img,ad083dd50b66adfb7637defd4c6aedd2ce9dfde6)
$(call add-radio-file-sha1-checked,radio/gpueb.img,6bb626789b690983bcf45e79a7a0501fbf803afb)
$(call add-radio-file-sha1-checked,radio/gz.img,1bd629b660c7078445391137b5d2fdc4f131da30)
$(call add-radio-file-sha1-checked,radio/lk.img,c878cffa4ee7dc04f3cd15386fe6f5c5591bde0a)
$(call add-radio-file-sha1-checked,radio/mcf_ota.img,0068ab4a836d6572b4329d0c9fef144b4bfdc69b)
$(call add-radio-file-sha1-checked,radio/mcupm.img,f3a906d84f69925d7e6c915d3d7c2f54748a7bc5)
$(call add-radio-file-sha1-checked,radio/md1img.img,3e57c98d42de67fb533d50bfe64efeddff054bef)
$(call add-radio-file-sha1-checked,radio/mvpu_algo.img,a94fffcc835e7c777ed71a9406cdebe5a3b4f656)
$(call add-radio-file-sha1-checked,radio/pi_img.img,930ffb49bd4447fdcb43e4c6bf9570ea978a3e22)
$(call add-radio-file-sha1-checked,radio/preloader_raw.img,d9399d4faeea64cf82b2675aa6b26bd8cbd75e36)
$(call add-radio-file-sha1-checked,radio/scp.img,2383b3f448f87e753c8afd81c07495dc2ce7d48e)
$(call add-radio-file-sha1-checked,radio/spmfw.img,9211d14b5c534f0a15802c39ff5662600ad7e884)
$(call add-radio-file-sha1-checked,radio/sspm.img,6b79c12cef4885f41873f15adc44b9ede0ef4d9d)
$(call add-radio-file-sha1-checked,radio/tee.img,52d2355a89260c8ad6d2315f5fa07b116f7e349d)
$(call add-radio-file-sha1-checked,radio/vcp.img,ef063162fcdfa57a03afd2ff3f280750f112f464)

endif
