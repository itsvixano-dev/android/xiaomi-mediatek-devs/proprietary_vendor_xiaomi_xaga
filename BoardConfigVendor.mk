#
# Automatically generated file. DO NOT MODIFY
#

AB_OTA_PARTITIONS += \
    apusys \
    audio_dsp \
    ccu \
    dpm \
    dtbo \
    gpueb \
    gz \
    lk \
    mcf_ota \
    mcupm \
    md1img \
    mvpu_algo \
    pi_img \
    preloader_raw \
    scp \
    spmfw \
    sspm \
    tee \
    vcp
